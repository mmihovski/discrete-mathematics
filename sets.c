#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define NUMBER_OF_RAND_NUMBERS 10

typedef struct set 
{
	unsigned int* array;
	unsigned int length;
} set;

set * initialize(unsigned int number_of_blocks);
void print(set * set);
unsigned int * random_generator(unsigned int number_of_rand_numbers);

set * _union(set * A, set * B);
set * intersection(set * A, set * B);
set * difference(set * A, set * B);
set * symmetric_difference(set * A, set * B);

int main(void)
{
	srand((unsigned int)time(NULL));
	set *A = initialize(3);
	set *B = initialize(6);

	printf("Set A:\t");
	print(A);
	printf("Set B:\t");
	print(B);

	printf("\nUnion between A and B is:\n");
	print(_union(A, B));
	printf("\nDifference between A and B is:\n");
	print(difference(A, B));
	printf("\nIntersection between A and B is:\n");
	print(intersection(A, B));
	printf("\nSymmetric difference between A and B is:\n");
	print(symmetric_difference(A, B));

	free(A);
	free(B);

	fflush(stdin);
	getchar();
	return 0;
}

set * initialize(unsigned int number_of_elements)
{
	set * _set = (set*)malloc(sizeof(set));
	_set->array = (unsigned int*)malloc(sizeof(unsigned int)*number_of_elements);
	_set->array = random_generator(number_of_elements);
	_set->length = number_of_elements;

	return _set;
}

void print(set * _set)
{
	unsigned int i;
	for (i = 0; i<_set->length; i++)
	{
		printf("%d\t", _set->array[i]);
	}
	printf("\n");
}

unsigned int* random_generator(unsigned int number_of_blocks)
{
	unsigned int *array = (unsigned int*)malloc(sizeof(unsigned int)*number_of_blocks);
	unsigned int x, i = 0;

	while (i<number_of_blocks)
	{
		unsigned int r = rand() % NUMBER_OF_RAND_NUMBERS + 1;

		for (x = 0; x<i; x++)
		{
			if (array[x] == r)
			{
				break;
			}
		}
		if (x == i)
		{
			array[i++] = r;
		}
	}
	return array;
}

set * _union(set *A, set *B)
{
	unsigned int i, j, k, flag = 0, index = 0;
	set *result_set = (set*)malloc(sizeof(set));
	result_set->array = (unsigned int*)malloc(sizeof(unsigned int)*(A->length + B->length));

	for (i = 0; i < A->length; i++)
	{
		result_set->array[index++] = A->array[i];
	}

	for (j = 0; j<B->length; j++)
	{
		flag = 1;

		for (k = 0; k < A->length; k++)
		{
			if (B->array[j] == A->array[k])
			{
				flag = 0;
				break;
			}
		}

		if (flag == 1)
		{
			result_set->array[index++] = B->array[j];
		}
	}
	result_set->length = index;
	return result_set;
}

set *intersection(set * A, set * B)
{
	unsigned int i, j, index = 0;
	set * result_set = (set*)malloc(sizeof(set));
	result_set->array = (unsigned int*)malloc(sizeof(unsigned int)*A->length);
	result_set->length = 0;

	for (i = 0; i<B->length; i++)
	{
		for (j = 0; j<A->length; j++)
		{
			if (A->array[j] == B->array[i])
			{
				result_set->array[index++] = A->array[j];
			}
		}
	}
	result_set->length = index;
	return result_set;
}

set *difference(set *A, set *B)
{
	unsigned int flag = 0, i, j, index = 0, length = A->length - intersection(A, B)->length;
	set * result_set = (set*)malloc(sizeof(set));
	result_set->array = (unsigned int*)malloc(sizeof(unsigned int)*length);
	result_set->length = length;

	for (i = 0; i<A->length; i++)
	{
		flag = 1;

		for (j = 0; j < B->length; j++)
		{
			if (A->array[i] == B->array[j])
			{
				flag = 0;
				break;
			}
		}

		if (flag == 1)
		{
			result_set->array[index++] = A->array[i];
		}
	}
	return result_set;
}

set * symmetric_difference(set * A, set * B)
{
	return _union(difference(A, B), difference(B, A));
}