﻿#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef struct set 
{
	unsigned int* array;
	unsigned int length;
} set;

void generate_permutations(unsigned int n);
void generate_combinations(unsigned int k, unsigned int n);
unsigned long long factoriel(unsigned int n);
unsigned long long combinations(unsigned int k, unsigned int n);
void swap(unsigned int * a, unsigned int * b);
set * create_set(unsigned n);
void print(set * _set);

int main(void)
{
	unsigned int n;

	printf("Enter n: ");
	scanf_s("%d", &n);

	printf("\nPermutation with %d numbers are:\n", n);
	generate_permutations(n);

	printf("\n");
	generate_combinations(3, 5);

	fflush(stdin);
	getchar();
	return 0;
}

set * create_set(unsigned int n)
{
	set * _set = (set*)malloc(sizeof(set));
	_set->array = (unsigned int*)malloc(sizeof(unsigned int)*n);
	_set->length = n;

	unsigned int i;
	for (i = 0; i<n; i++) 
	{
		_set->array[i] = i + 1;
	}

	return _set;
}

void print(set * _set)
{
	unsigned int i;
	for (i = 0; i<_set->length; i++) 
	{
		printf("%d  ", _set->array[i]);
	}
	printf("\n");
}

unsigned long long factoriel(unsigned int n) 
{

	unsigned long long s = 1, i;

	for (i = 2; i <= n; i++)
	{
		s = s*i;
	}
	return s;
}

void swap(unsigned int * a, unsigned int * b) 
{
	unsigned int temp = *a;
	*a = *b;
	*b = temp;
}

void generate_permutations(unsigned int n) 
{
	int i, m, k, p, q;
	set * _set = create_set(n);

	print(_set);

	for (i = 2; i <= factoriel(n); i++)
	{
		m = n - 2;
		while (*(_set->array + m) > *(_set->array + m + 1)) 
		{
			m = m - 1;
		}

		k = n - 1;

		while (*(_set->array + m) > *(_set->array + k)) 
		{
			k = k - 1;
		}

		swap(&*(_set->array + m), &*(_set->array + k));
		p = m + 1;
		q = n - 1;

		while (p < q)
		{
			swap(&*(_set->array + p), &*(_set->array + q));
			p = p + 1;
			q = q - 1;
		}
		print(_set);
	}
}

unsigned long long combinations(unsigned int k, unsigned int n) 
{
	return factoriel(n) / (factoriel(k)*factoriel(n - k));
}

void generate_combinations(unsigned int k, unsigned int n) 
{
	unsigned i, j, max_val, m;

	set * _set = create_set(k);

	print(_set);

	for (i = 2; i <= combinations(k, n); i++) 
	{
		m = k - 1;
		max_val = n;
		while (*(_set->array + m) == max_val) 
		{
			m = m - 1;
			max_val = max_val - 1;
		}
		*(_set->array + m) = *(_set->array + m) + 1;
		for (j = m + 1; j <= k; j++)
		{
			*(_set->array + j) = *(_set->array + j - 1) + 1;
		}

		print(_set);
	}
}