﻿#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define NUMBER_OF_RAND_NUMBERS 10

typedef struct set 
{
	unsigned int* array;
	unsigned int length;
} set;

set * initialize(unsigned int number_of_blocks);
void print(set * set);
unsigned int * random_generator(unsigned int number_of_rand_numbers);
void cartesian_product(set *set1, set *set2);


int main(void)
{
	srand((unsigned int)time(NULL));
	set *A = initialize(3);
	set *B = initialize(6);

	printf("Set A:\t");
	print(A);
	printf("Set B:\t");
	print(B);

	cartesian_product(A, B);

	free(A);
	free(B);

	fflush(stdin);
	getchar();
	return 0;
}

set * initialize(unsigned int number_of_elements)
{
	set * _set = (set*)malloc(sizeof(set));
	_set->array = (unsigned int*)malloc(sizeof(unsigned int)*number_of_elements);
	_set->array = random_generator(number_of_elements);
	_set->length = number_of_elements;

	return _set;
}

void print(set * _set)
{
	unsigned int i;
	for (i = 0; i<_set->length; i++)
	{
		printf("%d\t", _set->array[i]);
	}
	printf("\n");
}

unsigned int* random_generator(unsigned int number_of_blocks)
{
	unsigned int *array = (unsigned int*)malloc(sizeof(unsigned int)*number_of_blocks);
	unsigned int x, i = 0;

	while (i<number_of_blocks)
	{
		unsigned int r = rand() % NUMBER_OF_RAND_NUMBERS + 1;

		for (x = 0; x<i; x++)
		{
			if (array[x] == r)
			{
				break;
			}
		}
		if (x == i)
		{
			array[i++] = r;
		}
	}
	return array;
}

void cartesian_product(set *set1, set *set2)
{
	unsigned int i, j;

	printf("\n\nCartessian product = \n{\n");

	for (i = 0; i < set1->length; i++)
	{
		printf("\t");
		for (j = 0; j < set2->length; j++)
		{
			printf("(%d , %d), ", set1->array[i], set2->array[j]);
		}
		printf("\n");
	}
	printf("}");
}